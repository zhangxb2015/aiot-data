package cc.iotkit.data.service;

import cc.iotkit.data.model.TbAlertRecord;
import com.baomidou.mybatisplus.extension.service.IService;

public interface AlertRecordService extends IService<TbAlertRecord> {
}
