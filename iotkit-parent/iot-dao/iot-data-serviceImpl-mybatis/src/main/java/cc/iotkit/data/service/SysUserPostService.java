package cc.iotkit.data.service;

import cc.iotkit.data.model.TbSysUserPost;
import com.baomidou.mybatisplus.extension.service.IService;

public interface SysUserPostService extends IService<TbSysUserPost> {
}
