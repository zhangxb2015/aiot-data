package cc.iotkit.data.service;

import cc.iotkit.data.model.TbSysRoleDept;
import com.baomidou.mybatisplus.extension.service.IService;

public interface SysRoleDeptService extends IService<TbSysRoleDept> {
}
